import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthComponent } from './auth/auth.component';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {FormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddEditDeleteMovieComponent } from './add-edit-delete-movie/add-edit-delete-movie.component';
import {YouTubePlayerModule} from "@angular/youtube-player";
import {HttpClientModule} from "@angular/common/http";
import { HighlightDirective } from './highlight.directive';
import {AppRoutingModule} from "./app-router";


@NgModule({

  declarations: [                   //aici declaram componentele noastre prin modul iger sau lazy
    AppComponent,                   //lazy=toate comp sunt def in memorie
    AuthComponent,
    DashboardComponent,
    AddEditDeleteMovieComponent,
    HighlightDirective,


    //iger=def doar cand le accesam
  ],
  imports: [                           //aici avem modulele componentelor
    BrowserModule,                    //la iger sunt modulare
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    YouTubePlayerModule, // npm install @angular/youtube-player   >imi da eroare in terminal
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]              //apeleaza componenta principala de unde sa porneasca
})
export class AppModule { }
                                            //acest fisier este echivalentul lui main din java

