import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MovieManagerService {
  private movies: Array<any> = []
  private idKey: number = 0;

  constructor(private http: HttpClient) {
    /* this.movies.push({
       id: this.getIdKey(),                                //asta este pt a avea titlurile diferite
       title: `${this.movies.length} - Morometii`,        //cu aceste `` creem un string dinamic combinat intre variabile si text
                                                          // este echivalentul  this.movie.length+ " - Morometii" - title".adica dolar si acolade in loc de concatenare de stringuri
       year: 2008,
       description: "Description",
       director:"Director",
     });

     this.movies.push({
       id: this.getIdKey(),
       title: `${this.movies.length} - Morometii`,
       description: "Description",
       director:"Director",
     });

     this.movies.push({
       id: this.getIdKey(),
       title: `${this.movies.length} - Morometii`,
       description: "Description",
       director:"Director",
     });
   */
  }
  add(movie: any): any {
    // this.movies.push(movie);
    let body ={title: movie.title,
      description:movie.description,
      year:movie.year,
      director: movie.director}
    return this.http.post(`${environment.baseUrl}/api/movie`, body)
  }

  update(movie: any): any {
    let body ={
      id: movie.id,
      title: movie.title,
      description:movie.description,
      year:movie.year,
      director: movie.director}
    return this.http.patch(`${environment.baseUrl}/api/movie/${movie.id}`, body);
  }

  delete(movie: any): any {
   // this.movies = this.movies.filter((item) => item.id !== movie.id);    // ia toate elementele cu idul diferit de cel pe care il cautam noi si le salveaza in arrayul principal.il stergem scotandu-l pe el din lista
    return this.http.delete(`${environment.baseUrl}/api/movie/${movie.id}`);
  }

  get(): any {
   // return this.movies;
    return this.http.get(`${environment.baseUrl}/api/movie/`)
  }

  getById(id: number): any {
  /*  let items = this.movies.filter((item) => item.id == id)             //asa ne va returna primul element din lista
    if (items.length == 1) {
      return items[0];
    } else
     return null;
  */
    return this.http.get(`${environment.baseUrl}/api/movie/${id}`);

  }

  getIdKey(): number {
    return this.idKey += 1;
  }


}
