import { Component, OnInit } from '@angular/core';
import {MovieManagerService} from "../movie-manager.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
movies:Array<any>=[];
  selectedMovie : any;
  constructor(private movieManager:MovieManagerService) {
  }

  ngOnInit(): void {
   // this.movies=this.movieManager.get();
    this.getMovies();
  }

  getMovies():void{
   // this.movies=this.movieManager.get();
    this.movieManager.get().subscribe((response:any )=>{
      this.movies=response.data;
    })
  }
  onSelectMovie(movie : any ) : void {
    console.log("Select Movie");
    this.selectedMovie=movie;
  }
}
