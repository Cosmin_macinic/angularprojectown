import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient) {
  }

  login(email: String, password: String) {
    return  this.http.post(`${environment.baseUrl}/api/auth/login`, {email: email, password: password});
  }

  register(email: String, password: String) {
    return this.http.post(`${environment.baseUrl}/api/auth/register`, {email: email, password: password});
  }
}
