import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges,} from '@angular/core';
import {MovieManagerService} from "../movie-manager.service";

@Component({
  selector: 'app-add-edit-delete-movie',
  templateUrl: './add-edit-delete-movie.component.html',
  styleUrls: ['./add-edit-delete-movie.component.css']
})
export class AddEditDeleteMovieComponent implements OnInit, OnChanges {
  @Input() movie: any;
  @Output() clear: EventEmitter<any> = new EventEmitter<any>();

  title: String = "";
  description: String = "";
  year: String = "";
  director: String = "";

  constructor(private movieManager: MovieManagerService) {
  }

  ngOnInit(): void {
  }

  //metodele care au legatura cu butoanele au denumire cu on (onSave, odDelete..)


  onInit(): void {

  }

  onSave(): void {
    // Daca obiectul movie definit mai sus ca si input este diferit de null
    // inseamna ca vom face un update
    // Daca obiectul este null inseamna ca vom face un create (add)

    if (this.movie != null) {
      let body: any = {
        id: this.movie.id,
        title: this.title,
        description: this.description,
        year: this.year,
        director: this.director
      };
      this.movieManager.update(body).subscribe((response: any) => {
        console.log(response);
        this.onClear(); // Sterge datele din formular dupa update
      })
    } else {
      let body: any = {
        id: this.movieManager.getIdKey(),
        title: this.title,
        description: this.description,
        year: this.year,
        director: this.director
      };
      console.log(body);
      this.movieManager.add(body).subscribe((response: any) => {
        console.log(response);
        this.onClear();
      })

    }
  }


  onDelete(): void {
    this.movieManager.delete(this.movie).subscribe((response: any) => {
      console.log(response)
      this.onClear()
    });
  }

  onClear(): void {
    this.title = "";
    this.description = "";
    this.year = "";
    this.director = "";
    console.log("onClear pressed;")
    this.clear.emit(); // .emit() este metoda care declanseaza evenimentul de Output
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.movie != null) {
      console.log(this.movie);
      this.title = this.movie.title;
      this.description = this.movie.description;
      this.year = this.movie.year;
      this.director = this.movie.director;
    }

  }
}
